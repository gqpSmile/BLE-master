// pages/bleExample/bleExample.js
const app = getApp()
let commonBLE = require('../../utils/BLE/commonBLE.js'); //公共BLE
let commonBLEDatas = null; //保存当前页面对象


// 定义类型执行不同产品
let OptionEnumType = {
    Vending: 2, //售卖机
};


/**
 * 处理返回部分数据回调函数
 * obj: 传过来的数据
 * types: 当前操作类型 【可在`commBLE.js`查看定义的】
 */
let callBack = function(obj, types) {
    console.log("index.js回调函数" + obj, types);
    // commonBLE.clearCallBack();
    if (commonBLEDatas.data.currentMeasureType == 4) { //售卖机
        if (types == 10) {
            //给电量赋值
            commonBLEDatas.data.battery = obj;

            //调用开锁指令
            commonBLEDatas.successOpenLock();
        }
    }
}

/**
 * 处理错误码回调
 */
let errorCallBack = function(errorType, errorMessage) {
    // feedbackApi.hideTimerToast(); //清空loadding
    commonBLEDatas.deviceConnectFail(); //展示 设备连接失败
    if (errorType == 10000 || errorType == 10006) {
        //连接蓝牙
        commonBLE.setReconnectionActionType();
        commonBLE.setAsddErrorCallback(errorCallBack);
        commonBLE.setWriteError(writeError);
        if (commonBLEDatas.data.currentMeasureType == 4) {
            commonBLE.initBle(commonBLEDatas.data.macAdress, OptionEnumType.Vending, commonBLEDatas.data.currentSerial);
        }
    } else {
        commonBLE.bluetoothStatus(errorType);
    }
}


/**
 * 写入失败回调
 */
let writeError = function(writeE) {
    console.log('写入数据状态', writeE);
    //写入错误页面展示状态
    commonBLEDatas.setData({
        connectStatus: 1,
        isConnect: 2,
        clickFlags: false
    })

    if (writeE == 'w') {
        feedbackApi.hideTimerToast(); //清空loadding
        clearInterval(commonBLEDatas.downSecondId); //清空倒计时

        wx.showToast({
            title: '连接失败,请再次尝试(0)',
            icon: 'none'
        })
        commonBLE.closeBLEConnection();
    }
}


Page({

    /**
     * 页面的初始数据
     */
    data: {
        currentMeasureType: 2, //测试 当前类型售卖机
        macAdress: '3b3533115510', //mac地址(这里后台保存时没有:号的，可自行处理)
        currentSerial: '', //当前操作格子序号
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        commonBLEDatas = this;
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },

    /**
     * 连接BLE
     */
    connectVendingBLE: function() {
        let that = this;
        commonBLE.setConnectionActionType(callBack); //连接后操作回调
        commonBLE.setAsddErrorCallback(errorCallBack); //执行错误时错误码回调
        commonBLE.setWriteError(writeError); //写入数据错误回调
        if (that.data.currentMeasureType == 2) {
            commonBLE.initBle(that.data.macAdress, OptionEnumType.Vending, that.data.currentSerial);
        }
    },

    /**
     * 售卖机开锁成功后提交数据  并展示相关的ui
     */
    successOpenLock: function() {
        let that = this;
        // 调用开锁及处理回调数据
        commonBLE.openVendingLock(function(isOpenLock, obj) {
            console.log('处理开锁指令回调====》' + isOpenLock);
            commonBLE.clearCallBack();

            var tempStr = "失败";
            if (isOpenLock) {
                tempStr = "成功";
                // 提交数据并展示成功开锁后Ui
                that.showSuccessBack();
            } else {
                wx.showToast({
                    title: "开锁" + tempStr,
                    duration: 3000,
                    image: '/page/common/libs/assets/images/error.png',
                })

                that.deviceConnectFail(); //展示 设备连接失败
                //断开蓝牙连接
                commonBLE.closeBLEConnection();
            }
        });
    },

    /**
     * 设备连接失败
     */
    deviceConnectFail: function () {
        // feedbackApi.hideTimerToast(); //清空loadding
        clearInterval(this.downSecondId); //清空倒计时
        this.setData({
            descTxt2: '设备连接失败',
            connectStatus: 1,
            isConnect: 2,
            clickFlags: false
        })
    },
})