# BLE-master

#### 介绍
微信小程序开发”蓝牙BLE“案例

#### 软件架构
微信小程序


#### 安装教程

1.  直接下载使用

#### 使用说明

1. 在使用页面的`bleExample.js`中修改需连接设备的`mac`地址。

   ```javascript
   Page({
       /**
        * 页面的初始数据
        */
       data: {
           macAdress: '3b3533115510', //mac地址(这里后台保存时没有:号的，可自行处理)
       },
   })
   ```

2. 在`commonBLE.js`中需修改下`配置服务uuid`。 案例中是为了判断连接不同产品设备写的。 如果你的需求只是对接一家公司的设备， 直接在全局中写就可以， 这个判断就不用了。

   ```javascript
   /**
    * 区分不同类型的服务相关uuid
    * 1-->充电宝 2-->售卖机
    */
   function flagServiceIDFun(serviceType) {
       console.log('方法中拿到type======>', serviceType);
       if (serviceType == 1) {
           serviceUUID[0] = "0000*E0-00*0-*0*0-*0*0-00**5F9**4*B"; //主 service的uuid 列表
           writeUUID = "00*0**E2-00*0-*0*0-*0*0-00**5F9**4*B"; //写读 UUID
           notifyUUID = "00*0**E1-00*0-*0*0-*0*0-00**5F9**4*B"; //notify UUID
           filterServiceUUID = "*E0";
       } else if (serviceType == 2) {
           serviceUUID[0] = "0000*E0-00*0-*0*0-*0*0-00**5F9**4*B"; //主 service的uuid 列表
           writeUUID = "00*0**E2-00*0-*0*0-*0*0-00**5F9**4*B"; //写读 UUID
           notifyUUID = "00*0**E1-00*0-*0*0-*0*0-00**5F9**4*B"; //notify UUID
           filterServiceUUID = "*E0";
   
           //这里介绍用name匹配方法
           filterDeviceName = getNameMac(macAddress, 6, 'abc_'); //设备名称
       }
   }
   ```

3. 根据`自身需求`删除不必要的代码（因案例中有些字段是用来区分产品的）

4. 点击`连接蓝牙设备`按钮，调用蓝牙功能....

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

